#!/bin/sh

# Set GOPATH and PATH
export GOPATH="$HOME/go"
export PATH="$PATH:$GOPATH/bin"

# Permanently add to ~/.profile and ~/.zshrc
if ! grep -q "/home/vscode/go/bin" ~/.profile; then
  echo "export GOPATH=\$HOME/go" >> ~/.profile
  echo "export PATH=/home/vscode/go/bin:\$PATH" >> ~/.profile
fi

if ! grep -q "/home/vscode/go/bin" ~/.zshrc; then
  echo "export GOPATH=\$HOME/go" >> ~/.zshrc
  echo "export PATH=/home/vscode/go/bin:\$PATH" >> ~/.zshrc
fi

# Clone Mage repository and build
git clone https://github.com/magefile/mage "$HOME/mage"
cd "$HOME/mage" && go run bootstrap.go

# Clean up temporary clone
rm -rf "$HOME/mage"

# Verify installation
if command -v mage >/dev/null 2>&1; then
  echo "Mage installed successfully at $(command -v mage)"
else
  echo "Mage installation failed" >&2
  exit 1
fi
