import * as filesystem from "fs"

import {
  Provider,
  Cluster,
  ClusterArgs,
  types
} from "@deposition.cloud/pulumi-pvecluster"
import * as pulumi from "@pulumi/pulumi"

const output: {
  cluster?: {
    name?: pulumi.Output<string>
    status?: pulumi.Output<string>
  }
} = {}

const config = new pulumi.Config()
const pvecluster = new Provider("pvecluster")

const connectionsConfig =
  config.requireObject<types.input.ConnectionConfigArgs[]>("connections")
const clusterConfig =
  config.requireObject<types.input.ClusterConfigArgs>("cluster")

const nodesArgs: pulumi.Output<types.input.NodeArgs>[] = []

const cluster: pulumi.Output<Cluster> = pulumi
  .output(clusterConfig.nodes)
  .apply((nodesConfigs) => {
    for (const nodeConfig of nodesConfigs) {
      // Find a matching connection based on the node host
      const matchingConnection = connectionsConfig.find(
        (connection) => connection.host === nodeConfig.host
      )
      if (matchingConnection) {
        const privateKey: pulumi.Input<string> = pulumi
          .output(matchingConnection.privateKeyPath)
          .apply((privateKeyPath) => readKey(privateKeyPath))

        const nodeArgs: types.input.NodeArgs = {
          host: matchingConnection.host,
          connection: {
            host: matchingConnection.host,
            privateKey: pulumi.secret(privateKey)
          }
        }

        nodesArgs.push(pulumi.output(nodeArgs))
      }
    }

    const clusterArgs: ClusterArgs = {
      name: clusterConfig.name,
      nodes: nodesArgs
    }

    return new Cluster(`cluster-${clusterConfig.name}`, clusterArgs, {
      provider: pvecluster
    })
  })

output.cluster = {
  name: cluster.name,
  status: cluster.status
}

function readKey(filePath: string): string {
  try {
    return filesystem.readFileSync(filePath, "utf8").trim()
  } catch (error) {
    if (error instanceof Error) {
      throw new Error(
        `Failed to read file: ${filePath}. Error: ${error.message}`
      )
    } else {
      throw new Error(
        `Failed to read file: ${filePath}. An unknown error occurred.`
      )
    }
  }
}

export const out = output
