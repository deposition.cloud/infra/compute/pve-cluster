# NodeJS Example

Test NodeJS provider

### Working with Pulumi locally

Inside dev container:

```bash
pulumi login file:///workspaces/pulumi-pvecluster/
export PULUMI_CONFIG_PASSPHRASE_FILE=/workspaces/pulumi-pvecluster/pulumi.passphrase
pulumi stack init omega
pulumi stack select omega
pulumi up
```

Example to configure connections

```bash
pulumi config set --secret --path 'provider-pvecluster-native:connections[0].privateKeyPass' [your-password-0]
```
