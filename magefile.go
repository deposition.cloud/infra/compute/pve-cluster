//go:build mage
// +build mage

package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/magefile/mage/mg"
)

// Variables equivalent to the ones defined in the Makefile
var (
	SHELL            = "/bin/bash"
	PROJECT_NAME     = "Pulumi PVE Ceph Resource Provider"
	PACK             = "pvecluster"
	PACKDIR          = "sdk"
	PROJECT          = "github.com/deposition-cloud/pulumi-" + PACK
	ORG              = "deposition-cloud"
	NAME             = "pvecluster"
	REPOSITORY       = "github.com/deposition-cloud/pulumi-" + PACK
	NODE_MODULE_NAME = "@deposition.cloud/pulumi-" + PACK
	NUGET_PKG_NAME   = "DepositionCloud.PveCluster"
	PROVIDER         = "pulumi-resource-" + PACK
	VERSION          = ""
	PROVIDER_PATH    = "provider"
	VERSION_PATH     = PROVIDER_PATH + ".Version"
	GOPATH           = os.Getenv("GOPATH")
	WORKING_DIR, _   = os.Getwd()
	EXAMPLES_DIR     = filepath.Join(WORKING_DIR, "examples", "yaml")
	TESTPARALLELISM  = "4"
	OS               = runtime.GOOS
)

func init() {
	if GOPATH == "" {
		cmd := exec.Command("go", "env", "GOPATH")
		output, err := cmd.Output()
		if err != nil {
			log.Fatal("Unable to determine GOPATH")
		}
		GOPATH = strings.TrimSpace(string(output))
	}
	if VERSION == "" {
		VERSION = getPulumiVersion("")
	}
}

// Helper function to execute shell commands
func sh(name string, args ...string) error {
	cmd := exec.Command(name, args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

// Helper function to get the Pulumi version
func getPulumiVersion(lang string) string {
	args := []string{"get", "version"}
	if lang != "" {
		args = append(args, "--language", lang)
	}
	cmd := exec.Command("pulumictl", args...)
	output, err := cmd.Output()
	if err != nil {
		log.Fatalf("Unable to get version from pulumictl: %v", err)
	}
	return strings.TrimSpace(string(output))
}

// Prepare customizes the generic provider template to your specific provider
func Prepare() error {
	fmt.Println("Running prepare...")

	if NAME == "" {
		log.Fatal("NAME not set")
	}
	if REPOSITORY == "" {
		log.Fatal("REPOSITORY not set")
	}
	if ORG == "" {
		log.Fatal("ORG not set")
	}
	providerCmdPath := filepath.Join("provider", "cmd", "pulumi-resource-xyz")
	if _, err := os.Stat(providerCmdPath); os.IsNotExist(err) {
		fmt.Println("Project already prepared")
		return nil
	}

	// Rename the provider command directory
	newProviderCmdPath := filepath.Join("provider", "cmd", PROVIDER)
	if err := os.Rename(providerCmdPath, newProviderCmdPath); err != nil {
		return err
	}

	// Perform text replacements
	pathsToExclude := []string{".git", "./sdk"}
	findArgs := []string{"-type", "f", "!", "-name", "go.sum"}
	for _, path := range pathsToExclude {
		findArgs = append(findArgs, "!", "-path", fmt.Sprintf("./%s/*", path))
	}
	cmd := exec.Command("find", ".", strings.Join(findArgs, " "))
	output, err := cmd.Output()
	if err != nil {
		return err
	}
	files := strings.Split(string(output), "\n")

	replacements := []struct {
		search  string
		replace string
	}{
		{"github.com/pulumi/pulumi-xyz", REPOSITORY},
		{"[xX]yz", NAME},
		{"[aA]bc", ORG},
	}

	for _, file := range files {
		if file == "" {
			continue
		}
		for _, r := range replacements {
			sedArgs := []string{"-i.bak", fmt.Sprintf("s/%s/%s/g", r.search, r.replace), file}
			if OS == "darwin" {
				sedArgs = append([]string{"-i", ""}, sedArgs[2:]...)
			}
			err := sh("sed", sedArgs...)
			if err != nil {
				return err
			}
			// Remove backup files
			os.Remove(file + ".bak")
		}
	}
	return nil
}

// Ensure runs 'go mod tidy' in the provider, sdk, and tests directories
func Ensure() error {
	fmt.Println("Running ensure...")
	dirs := []string{"provider", "sdk", "tests"}
	for _, dir := range dirs {
		cmd := exec.Command("go", "mod", "tidy")
		cmd.Dir = dir
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		if err := cmd.Run(); err != nil {
			return err
		}
	}
	return nil
}

// Provider builds the provider binary with embedded version information
func Provider() error {
	fmt.Println("Building provider...")
	ldflags := fmt.Sprintf("-X %s/%s=%s", PROJECT, VERSION_PATH, VERSION)
	args := []string{"build", "-o", filepath.Join(WORKING_DIR, "bin", PROVIDER), "-ldflags", ldflags, fmt.Sprintf("%s/%s/cmd/%s", PROJECT, PROVIDER_PATH, PROVIDER)}
	cmd := exec.Command("go", args...)
	cmd.Dir = "provider"
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

// ProviderDebug builds the provider with debug flags
func ProviderDebug() error {
	fmt.Println("Building provider in debug mode...")
	ldflags := fmt.Sprintf("-X %s/%s=%s", PROJECT, VERSION_PATH, VERSION)
	gcflags := "all=-N -l"
	args := []string{"build", "-o", filepath.Join(WORKING_DIR, "bin", PROVIDER), "-gcflags", gcflags, "-ldflags", ldflags, fmt.Sprintf("%s/%s/cmd/%s", PROJECT, PROVIDER_PATH, PROVIDER)}
	cmd := exec.Command("go", args...)
	cmd.Dir = "provider"
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

// TestProvider runs the provider tests
func TestProvider() error {
	fmt.Println("Running provider tests...")
	args := []string{"test", "-short", "-v", "-count=1", "-cover", "-timeout", "2h", "-parallel", TESTPARALLELISM, "./..."}
	cmd := exec.Command("go", args...)
	cmd.Dir = "tests"
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

// DotnetSDK generates the .NET SDK for the provider
func DotnetSDK() error {
	fmt.Println("Generating .NET SDK...")
	DOTNET_VERSION := getPulumiVersion("dotnet")
	sdkDir := filepath.Join(PACKDIR, "dotnet")
	os.RemoveAll(sdkDir)
	if err := sh("pulumi", "package", "gen-sdk", filepath.Join(WORKING_DIR, "bin", PROVIDER), "--language", "dotnet"); err != nil {
		return err
	}
	versionFile := filepath.Join(sdkDir, "version.txt")
	if err := os.WriteFile(versionFile, []byte(DOTNET_VERSION), 0644); err != nil {
		return err
	}
	cmd := exec.Command("dotnet", "build", fmt.Sprintf("/p:Version=%s", DOTNET_VERSION))
	cmd.Dir = sdkDir
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

// GoSDK generates the Go SDK for the provider
func GoSDK() error {
	fmt.Println("Generating Go SDK...")
	sdkDir := filepath.Join(PACKDIR, "go")
	os.RemoveAll(sdkDir)
	if err := sh("pulumi", "package", "gen-sdk", filepath.Join(WORKING_DIR, "bin", PROVIDER), "--language", "go"); err != nil {
		return err
	}
	return nil
}

func NodejsSDK() error {
	fmt.Println("Generating Node.js SDK...")
	VERSION := getPulumiVersion("javascript")
	sdkDir := filepath.Join(PACKDIR, "nodejs")
	os.RemoveAll(sdkDir)
	if err := sh("pulumi", "package", "gen-sdk", filepath.Join(WORKING_DIR, "bin", PROVIDER), "--language", "nodejs"); err != nil {
		return err
	}
	// Update package.json in sdkDir
	packageJSON := filepath.Join(sdkDir, "package.json")
	input, err := os.ReadFile(packageJSON)
	if err != nil {
		return fmt.Errorf("failed to read %s: %w", packageJSON, err)
	}
	content := strings.Replace(string(input), fmt.Sprintf("@pulumi/%s", PACK), NODE_MODULE_NAME, -1)
	if err := os.WriteFile(packageJSON, []byte(content), 0644); err != nil {
		return err
	}
	// Run yarn commands in sdkDir
	cmd := exec.Command("yarn", "install")
	cmd.Dir = sdkDir
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return err
	}
	cmd = exec.Command("yarn", "run", "tsc")
	cmd.Dir = sdkDir
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return err
	}
	// Copy README.md and LICENSE from WORKING_DIR, and package.json and yarn.lock from sdkDir to bin
	binDir := filepath.Join(sdkDir, "bin")
	os.MkdirAll(binDir, 0755)
	filesToCopy := []struct {
		SrcDir   string
		FileName string
	}{
		{WORKING_DIR, "README.md"},
		{WORKING_DIR, "LICENSE"},
		{sdkDir, "package.json"},
		{sdkDir, "yarn.lock"},
	}
	for _, file := range filesToCopy {
		src := filepath.Join(file.SrcDir, file.FileName)
		dst := filepath.Join(binDir, file.FileName)
		input, err := os.ReadFile(src)
		if err != nil {
			return fmt.Errorf("failed to read %s: %w", src, err)
		}
		if err := os.WriteFile(dst, input, 0644); err != nil {
			return fmt.Errorf("failed to write %s: %w", dst, err)
		}
	}
	// Update version in bin/package.json
	packageJSONBin := filepath.Join(binDir, "package.json")
	input, err = os.ReadFile(packageJSONBin)
	if err != nil {
		return err
	}
	content = strings.Replace(string(input), "${VERSION}", VERSION, -1)
	if err := os.WriteFile(packageJSONBin, []byte(content), 0644); err != nil {
		return err
	}
	return nil
}

// PythonSDK generates the Python SDK for the provider
func PythonSDK() error {
	fmt.Println("Generating Python SDK...")
	PYPI_VERSION := getPulumiVersion("python")
	sdkDir := filepath.Join(PACKDIR, "python")
	os.RemoveAll(sdkDir)
	if err := sh("pulumi", "package", "gen-sdk", filepath.Join(WORKING_DIR, "bin", PROVIDER), "--language", "python"); err != nil {
		return err
	}
	// Copy README.md
	if err := sh("cp", "README.md", sdkDir); err != nil {
		return err
	}
	// Build the package
	cmd := exec.Command("python3", "setup.py", "clean", "--all")
	cmd.Dir = sdkDir
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Run()
	// Move files to bin directory
	binDir := filepath.Join(sdkDir, "bin")
	os.RemoveAll(binDir)
	os.MkdirAll(binDir, 0755)
	if err := filepath.Walk(sdkDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		// Skip the bin directory itself
		if path == binDir {
			return filepath.SkipDir
		}
		relPath, _ := filepath.Rel(sdkDir, path)
		dstPath := filepath.Join(binDir, relPath)
		if info.IsDir() {
			return os.MkdirAll(dstPath, info.Mode())
		}
		input, err := os.ReadFile(path)
		if err != nil {
			return err
		}
		return os.WriteFile(dstPath, input, info.Mode())
	}); err != nil {
		return err
	}
	// Update setup.py with versions
	setupPy := filepath.Join(binDir, "setup.py")
	input, err := os.ReadFile(setupPy)
	if err != nil {
		return err
	}
	content := string(input)
	content = strings.Replace(content, `VERSION = ""`, fmt.Sprintf(`VERSION = "%s"`, PYPI_VERSION), -1)
	content = strings.Replace(content, `PLUGIN_VERSION = ""`, fmt.Sprintf(`PLUGIN_VERSION = "%s"`, VERSION), -1)
	if err := os.WriteFile(setupPy, []byte(content), 0644); err != nil {
		return err
	}
	// Build the source distribution
	cmd = exec.Command("python3", "setup.py", "build", "sdist")
	cmd.Dir = binDir
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

// GenExamples generates code examples in multiple languages from YAML examples
func GenExamples() error {
	fmt.Println("Generating examples...")
	languages := []string{"go", "nodejs", "python", "dotnet"}
	for _, lang := range languages {
		if err := GenExample(lang); err != nil {
			return err
		}
	}
	return nil
}

// GenExample generates code examples for a specific language
func GenExample(lang string) error {
	fmt.Printf("Generating %s example...\n", lang)
	exampleDir := filepath.Join(WORKING_DIR, "examples", lang)
	os.RemoveAll(exampleDir)
	args := []string{
		"convert",
		"--cwd", EXAMPLES_DIR,
		"--logtostderr",
		"--generate-only",
		"--non-interactive",
		"--language", lang,
		"--out", exampleDir,
	}
	cmd := exec.Command("pulumi", args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

// Up deploys the stack
func Up() error {
	fmt.Println("Running pulumi up...")
	if err := pulumiLogin(); err != nil {
		return err
	}

	stackName := "omega"        // Replace with your desired stack name
	examplesDir := EXAMPLES_DIR // Adjust if necessary

	// Attempt to select or create the stack
	if err := pulumiSelectOrCreateStack(examplesDir, stackName); err != nil {
		return fmt.Errorf("failed to select or create Pulumi stack: %w", err)
	}

	// Set any necessary configuration
	cmd := pulumiCmd(examplesDir, "config", "set", "name", stackName)
	if err := cmd.Run(); err != nil {
		return err
	}

	// Run 'pulumi up'
	cmd = pulumiCmd(examplesDir, "up", "-y")
	if err := cmd.Run(); err != nil {
		return err
	}

	return nil
}

// Down destroys and removes the stack
func Down() error {
	fmt.Println("Running pulumi down...")
	if err := pulumiLogin(); err != nil {
		return err
	}
	cmds := [][]string{
		{"stack", "select", "omega"},
		{"destroy", "-y"},
		{"stack", "rm", "omega", "-y"},
	}
	for _, args := range cmds {
		cmd := exec.Command("pulumi", args...)
		cmd.Dir = EXAMPLES_DIR
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		if err := cmd.Run(); err != nil {
			return err
		}
	}
	return nil
}

// Devcontainer updates and copies development container configurations
func Devcontainer() error {
	fmt.Println("Updating devcontainer...")
	if err := sh("git", "submodule", "update", "--init", "--recursive", ".devcontainer"); err != nil {
		return err
	}
	if err := sh("git", "submodule", "update", "--remote", "--merge", ".devcontainer"); err != nil {
		return err
	}
	if err := sh("cp", "-f", ".devcontainer/devcontainer.json", ".devcontainer.json"); err != nil {
		return err
	}
	return nil
}

// Build builds the provider and all SDKs
func Build() {
	mg.Deps(Provider, DotnetSDK, GoSDK, NodejsSDK, PythonSDK)
}

// OnlyBuild is an alias for Build, possibly used in CI pipelines
func OnlyBuild() {
	mg.Deps(Build)
}

// Lint runs linters on Go code in specified directories
func Lint() error {
	fmt.Println("Running linters...")
	dirs := []string{"provider", "sdk", "tests"}
	for _, dir := range dirs {
		cmd := exec.Command("golangci-lint", "run", "-c", "../.golangci.yml", "--timeout", "10m")
		cmd.Dir = dir
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		if err := cmd.Run(); err != nil {
			return err
		}
	}
	return nil
}

// Install installs the provider binary and SDKs
func Install() error {
	mg.Deps(InstallNodejsSDK, InstallDotnetSDK)
	// Copy provider binary to GOPATH/bin
	dest := filepath.Join(GOPATH, "bin", PROVIDER)
	src := filepath.Join(WORKING_DIR, "bin", PROVIDER)
	if err := sh("cp", src, dest); err != nil {
		return err
	}
	return nil
}

// TestAll runs tests across all SDKs
func TestAll() error {
	fmt.Println("Running all tests...")
	mg.Deps(TestProvider)
	testDirs := []string{
		"tests/sdk/nodejs",
		"tests/sdk/python",
		"tests/sdk/dotnet",
		"tests/sdk/go",
	}
	for _, dir := range testDirs {
		args := []string{"test", "-v", "-count=1", "-cover", "-timeout", "2h", "-parallel", TESTPARALLELISM, "./..."}
		cmd := exec.Command("go", args...)
		cmd.Dir = dir
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		if err := cmd.Run(); err != nil {
			return err
		}
	}
	return nil
}

// InstallDotnetSDK copies NuGet packages to a local directory
func InstallDotnetSDK() error {
	fmt.Println("Installing .NET SDK...")
	nugetDir := filepath.Join(WORKING_DIR, "nuget")
	os.RemoveAll(nugetDir)
	os.MkdirAll(nugetDir, 0755)
	err := filepath.Walk(".", func(path string, info os.FileInfo, err error) error {
		if strings.HasSuffix(path, ".nupkg") {
			dest := filepath.Join(nugetDir, filepath.Base(path))
			return sh("cp", "-p", path, dest)
		}
		return nil
	})
	return err
}

// InstallPythonSDK is intentionally left blank
func InstallPythonSDK() {
	// Target intentionally blank
}

// InstallGoSDK is intentionally left blank
func InstallGoSDK() {
	// Target intentionally blank
}

func pulumiCmd(dir string, args ...string) *exec.Cmd {
	cmd := exec.Command("pulumi", args...)
	cmd.Dir = dir
	cmd.Env = append(os.Environ(), "PULUMI_CONFIG_PASSPHRASE_FILE=../../pulumi.passphrase")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd
}

func pulumiLogin() error {
	cmd := pulumiCmd("", "login", "--local")
	return cmd.Run()
}

// InstallNodejsSDK links the Node.js SDK for local development
func InstallNodejsSDK() error {
	fmt.Println("Installing Node.js SDK...")
	sdkBinDir := filepath.Join(WORKING_DIR, "sdk", "nodejs", "bin")
	examplesNodejsDir := filepath.Join(WORKING_DIR, "examples", "nodejs")

	// Unlink if already linked
	cmd := exec.Command("yarn", "unlink")
	cmd.Dir = sdkBinDir
	cmd.Run() // Ignore errors in case it's not linked yet

	// Link the SDK globally
	cmd = exec.Command("yarn", "link")
	cmd.Dir = sdkBinDir
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return err
	}

	// Link the example project to the SDK
	cmd = exec.Command("yarn", "link", NODE_MODULE_NAME)
	cmd.Dir = examplesNodejsDir
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return err
	}

	// Install dependencies in the example project
	cmd = exec.Command("yarn", "install")
	cmd.Dir = examplesNodejsDir
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return err
	}

	return nil
}

func DevNodejs() error {
	fmt.Println("Starting Node.js development workflow...")

	if err := Provider(); err != nil {
		return fmt.Errorf("failed to build provider: %w", err)
	}

	if err := NodejsSDK(); err != nil {
		return fmt.Errorf("failed to build Node.js SDK: %w", err)
	}

	if err := InstallNodejsSDK(); err != nil {
		return fmt.Errorf("failed to install Node.js SDK: %w", err)
	}

	examplesDir := filepath.Join(WORKING_DIR, "examples", "nodejs")
	if _, err := os.Stat(examplesDir); os.IsNotExist(err) {
		return errors.New("examples/nodejs directory does not exist")
	}

	if err := pulumiLogin(); err != nil {
		return fmt.Errorf("failed to login to Pulumi: %w", err)
	}

	stackName := "omega" // Use the stack name you intend to use
	if err := pulumiSelectOrCreateStack(examplesDir, stackName); err != nil {
		return fmt.Errorf("failed to select or create Pulumi stack: %w", err)
	}

	fmt.Println("Destroying existing stack...")
	if err := pulumiDestroy(examplesDir); err != nil {
		// Log the error but continue to attempt deployment
		fmt.Printf("Pulumi destroy encountered an error: %v\n", err)
	}

	fmt.Println("Deploying stack...")
	if err := pulumiUp(examplesDir); err != nil {
		return fmt.Errorf("failed to deploy stack: %w", err)
	}

	fmt.Println("Node.js development workflow completed successfully.")
	return nil
}

// Helper function to select or create a Pulumi stack
func pulumiSelectOrCreateStack(dir, stackName string) error {
	// Try to select the stack
	cmd := pulumiCmd(dir, "stack", "select", stackName)
	if err := cmd.Run(); err != nil {
		// If the stack doesn't exist, create it
		cmd := pulumiCmd(dir, "stack", "init", stackName)
		if err := cmd.Run(); err != nil {
			return err
		}
	}
	return nil
}

// Helper function to run 'pulumi destroy'
func pulumiDestroy(dir string) error {
	cmd := pulumiCmd(dir, "destroy", "-y")
	return cmd.Run()
}

// Helper function to run 'pulumi up'
func pulumiUp(dir string) error {
	cmd := pulumiCmd(dir, "up", "-y")
	return cmd.Run()
}
