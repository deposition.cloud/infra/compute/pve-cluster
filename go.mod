module github.com/deposition-cloud/pulumi-pvecluster

go 1.20

require (
	github.com/magefile/mage v1.15.0
	golang.org/x/crypto v0.29.0
)

require golang.org/x/sys v0.27.0 // indirect
