// *** WARNING: this file was generated by pulumi. ***
// *** Do not edit by hand unless you're certain you know what you are doing! ***

using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Threading.Tasks;
using Pulumi.Serialization;

namespace Pulumi.Pvecluster
{
    [PveclusterResourceType("pvecluster:index:Node")]
    public partial class Node : global::Pulumi.CustomResource
    {
        [Output("config")]
        public Output<Outputs.NodeConfig?> Config { get; private set; } = null!;

        [Output("connection")]
        public Output<Outputs.Connection> Connection { get; private set; } = null!;

        [Output("disks")]
        public Output<Outputs.DiskSpec> Disks { get; private set; } = null!;

        [Output("host")]
        public Output<string> Host { get; private set; } = null!;

        [Output("status")]
        public Output<string> Status { get; private set; } = null!;


        /// <summary>
        /// Create a Node resource with the given unique name, arguments, and options.
        /// </summary>
        ///
        /// <param name="name">The unique name of the resource</param>
        /// <param name="args">The arguments used to populate this resource's properties</param>
        /// <param name="options">A bag of options that control this resource's behavior</param>
        public Node(string name, NodeArgs args, CustomResourceOptions? options = null)
            : base("pvecluster:index:Node", name, args ?? new NodeArgs(), MakeResourceOptions(options, ""))
        {
        }

        private Node(string name, Input<string> id, CustomResourceOptions? options = null)
            : base("pvecluster:index:Node", name, null, MakeResourceOptions(options, id))
        {
        }

        private static CustomResourceOptions MakeResourceOptions(CustomResourceOptions? options, Input<string>? id)
        {
            var defaultOptions = new CustomResourceOptions
            {
                Version = Utilities.Version,
            };
            var merged = CustomResourceOptions.Merge(defaultOptions, options);
            // Override the ID if one was specified for consistency with other language SDKs.
            merged.Id = id ?? merged.Id;
            return merged;
        }
        /// <summary>
        /// Get an existing Node resource's state with the given name, ID, and optional extra
        /// properties used to qualify the lookup.
        /// </summary>
        ///
        /// <param name="name">The unique name of the resulting resource.</param>
        /// <param name="id">The unique provider ID of the resource to lookup.</param>
        /// <param name="options">A bag of options that control this resource's behavior</param>
        public static Node Get(string name, Input<string> id, CustomResourceOptions? options = null)
        {
            return new Node(name, id, options);
        }
    }

    public sealed class NodeArgs : global::Pulumi.ResourceArgs
    {
        [Input("config")]
        public Input<Inputs.NodeConfigArgs>? Config { get; set; }

        [Input("connection", required: true)]
        public Input<Inputs.ConnectionArgs> Connection { get; set; } = null!;

        [Input("disks", required: true)]
        public Input<Inputs.DiskSpecArgs> Disks { get; set; } = null!;

        [Input("host", required: true)]
        public Input<string> Host { get; set; } = null!;

        public NodeArgs()
        {
        }
        public static new NodeArgs Empty => new NodeArgs();
    }
}
