package provider

import (
	"github.com/deposition-cloud/pulumi-pvecluster/provider/resources"

	p "github.com/pulumi/pulumi-go-provider"
	"github.com/pulumi/pulumi-go-provider/infer"
	"github.com/pulumi/pulumi/sdk/v3/go/common/tokens"
)

var Version string

const Name string = "pvecluster"

func Provider() p.Provider {
	return infer.Provider(infer.Options{
		Resources: []infer.InferredResource{
			infer.Resource[resources.Cluster, resources.ClusterArgs, resources.ClusterState](),
		},
		ModuleMap: map[tokens.ModuleName]tokens.ModuleName{
			"provider":  "index",
			"resources": "index",
		},
	})
}
