package log

import (
	"fmt"
	"sync"

	p "github.com/pulumi/pulumi-go-provider"
	"github.com/pulumi/pulumi/sdk/v3/go/common/diag"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

var (
	Info    = createLogFunction(info)
	Warn    = createLogFunction(warn)
	Error   = createLogFunction(err)
	Debug   = createLogFunction(debug)
	InfoErr = createLogFunction(infoErr)
)

type LogLevel string

const (
	info    LogLevel = "info"
	warn    LogLevel = "warning"
	err     LogLevel = "error"
	debug   LogLevel = "debug"
	infoErr LogLevel = "infoerr"
)

const (
	LevelInfo    = info
	LevelWarn    = warn
	LevelError   = err
	LevelDebug   = debug
	LevelInfoErr = infoErr
)

// Function map for pulumi.Context logging methods
var pulumiLogMap = map[LogLevel]func(*pulumi.Context, string){
	info:    func(ctx *pulumi.Context, message string) { ctx.Log.Info(message, &pulumi.LogArgs{}) },
	warn:    func(ctx *pulumi.Context, message string) { ctx.Log.Warn(message, &pulumi.LogArgs{}) },
	err:     func(ctx *pulumi.Context, message string) { ctx.Log.Error(message, &pulumi.LogArgs{}) },
	debug:   func(ctx *pulumi.Context, message string) { ctx.Log.Debug(message, &pulumi.LogArgs{}) },
	infoErr: func(ctx *pulumi.Context, message string) { ctx.Log.Info(message, &pulumi.LogArgs{Ephemeral: true}) },
}

// Function map for p.Context logging methods
var pLogMap = map[LogLevel]func(p.Context, string){
	info:    func(ctx p.Context, message string) { ctx.Log(diag.Info, message) },
	warn:    func(ctx p.Context, message string) { ctx.Log(diag.Warning, message) },
	err:     func(ctx p.Context, message string) { ctx.Log(diag.Error, message) },
	debug:   func(ctx p.Context, message string) { ctx.Log(diag.Debug, message) },
	infoErr: func(ctx p.Context, message string) { ctx.Log(diag.Infoerr, message) },
}

// Function to dynamically generate log functions
func createLogFunction(level LogLevel) func(ctx interface{}, message string) {
	return func(ctx interface{}, message string) {
		threadSafeLog(ctx, message, level)
	}
}

var logMutex sync.Mutex

func threadSafeLog(ctx interface{}, message string, level LogLevel) {
	logMutex.Lock()
	defer logMutex.Unlock()
	genericLog(ctx, message, level)
}

// Generic log function for both context types
func genericLog(ctx interface{}, message string, level LogLevel) {
	switch c := ctx.(type) {
	case *pulumi.Context:
		fmt.Printf("Using pulumi.Context for logging with level %s\n", level)
		if logFunc, ok := pulumiLogMap[level]; ok {
			logFunc(c, message)
		} else {
			fmt.Printf("Unknown log level for pulumi.Context: %s\n", level)
		}
	case p.Context:
		fmt.Printf("Using p.Context for logging with level %s\n", level)
		if logFunc, ok := pLogMap[level]; ok {
			logFunc(c, message)
		} else {
			fmt.Printf("Unknown log level for p.Context: %s\n", level)
		}
	default:
		// Fallback to fmt.Printf if context is not recognized
		fmt.Printf("Fallback logging [%s]: %s\n", level, message)
	}
}
