package resources

import (
	"fmt"
	"net"
	"time"

	"github.com/deposition-cloud/pulumi-pvecluster/provider/log"
	"golang.org/x/crypto/ssh"
)

// Connection defines the structure for SSH connection information with optional passphrase and port
type Connection struct {
	Host           string  `pulumi:"host"`
	User           *string `pulumi:"user,optional"`
	PrivateKey     string  `pulumi:"privateKey"`
	PrivateKeyPass string  `pulumi:"privateKeyPass,optional"`
	Port           *int    `pulumi:"port,optional"`
}

type ConnectionConfig struct {
	Host           string `pulumi:"host"`
	User           string `pulumi:"user,optional"`
	PrivateKeyPath string `pulumi:"privateKeyPath"`
	PrivateKeyPass string `pulumi:"privateKeyPass,optional"`
	Port           int    `pulumi:"port,optional"`
}

// LogEntry holds the log level and message
type LogEntry struct {
	Level   log.LogLevel
	Message string
}

// ExecuteCommand runs a command over SSH using in-memory private key content with optional passphrase and port
func (conn *Connection) ExecuteCommand(ctx interface{}, command string) error {
	var inputs []interface{}
	inputs = append(inputs, conn.PrivateKey, conn.PrivateKeyPass, conn.Host, conn.User, conn.Port)

	if inputs == nil || len(inputs) != 5 {
		log.Error(ctx, "Input resolution failed or not enough inputs")
		return fmt.Errorf("input resolution failed or not enough inputs")
	}
	log.Info(ctx, "Inputs successfully retrieved")

	host, ok := inputs[2].(string)
	if !ok || host == "" {
		log.Error(ctx, "Host is required and must be a non-empty string")
		return fmt.Errorf("host is required and must be a non-empty string")
	}
	log.Info(ctx, fmt.Sprintf("Host extracted: %s", host))

	privateKey, ok := inputs[0].(string)
	if !ok || privateKey == "" {
		log.Error(ctx, "PrivateKey is required and must be a non-empty string")
		return fmt.Errorf("PrivateKey is required and must be a non-empty string")
	}
	log.Info(ctx, "PrivateKey successfully retrieved")

	privateKeyPass := ""
	if inputs[1] != nil {
		privateKeyPass, _ = inputs[1].(string)
	}

	user := "root"
	if inputs[3] != nil {
		if val, ok := inputs[3].(*string); ok && val != nil && *val != "" {
			user = *val
		}
	}
	log.Info(ctx, fmt.Sprintf("User extracted: %s", user))

	port := 22
	if inputs[4] != nil {
		if val, ok := inputs[4].(*int); ok && val != nil && *val != 0 {
			port = *val
		}
	}
	log.Info(ctx, fmt.Sprintf("Port extracted: %d", port))

	log.Info(ctx, "All inputs successfully validated")

	var signer ssh.Signer
	var err error
	keyBytes := []byte(privateKey)

	if privateKeyPass != "" {
		signer, err = ssh.ParsePrivateKeyWithPassphrase(keyBytes, []byte(privateKeyPass))
		if err != nil {
			log.Error(ctx, fmt.Sprintf("Failed to parse private key with passphrase: %v", err))

			return err
		}
	} else {
		signer, err = ssh.ParsePrivateKey(keyBytes)
		if err != nil {
			log.Error(ctx, fmt.Sprintf("Failed to parse private key: %v", err))

			return err
		}
	}
	log.Info(ctx, "Private key successfully parsed")

	config := &ssh.ClientConfig{
		User: user,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(signer),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(), // Warning: replace with proper host key verification in production
	}

	address := fmt.Sprintf("%s:%d", host, port)
	log.Info(ctx, fmt.Sprintf("Attempting to dial SSH at %s", address))

	// Use net.Dialer with timeout
	netDialer := &net.Dialer{
		Timeout: 10 * time.Second,
	}

	dialConn, err := netDialer.Dial("tcp", address)
	if err != nil {
		log.Error(ctx, fmt.Sprintf("Failed to dial: %v", err))

		return err
	}
	log.Info(ctx, fmt.Sprintf("Dialed successfully to %s", address))

	clientConn, chans, reqs, err := ssh.NewClientConn(dialConn, address, config)
	if err != nil {
		log.Error(ctx, fmt.Sprintf("Failed to create SSH client connection: %v", err))

		return err
	}
	log.Info(ctx, "SSH client connection created successfully")

	client := ssh.NewClient(clientConn, chans, reqs)
	defer client.Close()

	session, err := client.NewSession()
	if err != nil {
		log.Error(ctx, fmt.Sprintf("Failed to create SSH session: %v", err))

		return err
	}
	log.Info(ctx, "New SSH session created")
	defer session.Close()

	// Run the command and capture both stdout and stderr in a single output
	output, err := session.CombinedOutput(command)
	if err != nil {
		log.Error(ctx, fmt.Sprintf("Failed to run SSH command: %v", err))
		log.Error(ctx, fmt.Sprintf("Command output (including stderr): %s", output))

		return err
	}

	log.Info(ctx, "SSH command executed successfully")
	log.Info(ctx, fmt.Sprintf("Command output: %s", output))

	fmt.Println("Emitting logs...") // Debugging step

	// Return any SSH error encountered
	return nil
}
