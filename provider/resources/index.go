package resources

import (
	p "github.com/pulumi/pulumi-go-provider"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

type CompleteResource[Args any, State any] interface {
	Create(ctx p.Context, name string, input Args, preview bool) (string, State, error)
	Read(ctx *pulumi.Context, id pulumi.IDInput, state State) (State, error)
	Update(ctx *pulumi.Context, id pulumi.IDInput, args Args) (State, error)
	Delete(ctx *pulumi.Context, id pulumi.IDInput, args Args) error
	// Check(ctx p.Context, name string, olds State, news Args) (Args, []p.CheckFailure, error)
	// Diff(ctx p.Context, id string, olds State, news Args) (p.DiffResponse, error)
}
