package resources

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"sort"
	"strings"

	"github.com/deposition-cloud/pulumi-pvecluster/provider/log"
	p "github.com/pulumi/pulumi-go-provider"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

type Cluster struct {
	pulumi.CustomResourceState

	Name   pulumi.StringOutput `pulumi:"name"`
	Nodes  []Node              `pulumi:"nodes"`
	Status pulumi.StringOutput `pulumi:"status"`
}

var _ CompleteResource[ClusterArgs, ClusterState] = &Cluster{}

type Node struct {
	Host       string      `pulumi:"host"`
	Connection *Connection `pulumi:"connection"`
}

type ClusterConfig struct {
	Name  string       `pulumi:"name"`
	Nodes []NodeConfig `pulumi:"nodes"`
}

type NodeConfig struct {
	Host       string            `pulumi:"host"`
	Connection *ConnectionConfig `pulumi:"connection,optional"`
}

type ClusterArgs struct {
	Config *ClusterConfig `pulumi:"config,optional"`
	Name   string         `pulumi:"name"`
	Nodes  []Node         `pulumi:"nodes"`
}

type ClusterState struct {
	ClusterArgs
	Status string `pulumi:"status"`
}

// Create creates a new PVE cluster resource.
func (Cluster) Create(ctx p.Context, name string, input ClusterArgs, preview bool) (string, ClusterState, error) {
	if len(input.Name) > 15 {
		return "", ClusterState{}, fmt.Errorf("cluster name `%s` exceeds the 15 character limit required by `pvecm`", input.Name)
	}

	if preview {
		return name, ClusterState{
			Status: "preview",
		}, nil
	}

	if len(input.Nodes) == 0 {
		return "", ClusterState{}, fmt.Errorf("no nodes provided to create the cluster")
	}

	// Create the cluster on the main node
	log.Info(ctx, fmt.Sprintf("Creating PVE cluster: %s on node %s\n", input.Name, input.Nodes[0].Host))
	mainNode, err := createCluster(ctx, input)
	if err != nil {
		return "", ClusterState{}, err
	}

	// Add remaining nodes to the cluster
	for _, node := range input.Nodes[1:] {
		log.Info(ctx, fmt.Sprintf("Adding node %s to cluster %s\n", node.Host, input.Name))
		if err := addNodeToCluster(ctx, mainNode, node); err != nil {
			return "", ClusterState{}, err
		}
	}

	return name, ClusterState{
		ClusterArgs: input,
		Status:      "created",
	}, nil
}

// Read fetches the current state of the cluster and updates the ClusterState with real data.
func (n *Cluster) Read(ctx *pulumi.Context, id pulumi.IDInput, state ClusterState) (ClusterState, error) {
	// Simulate reading the cluster state
	log.Info(ctx, fmt.Sprintf("Fetching state of cluster: %s\n", state.Name))

	// Run a command to get the current cluster status
	cmd := "pvecm status"
	if err := state.Nodes[0].Connection.ExecuteCommand(*ctx, cmd); err != nil {
		return ClusterState{}, fmt.Errorf("failed to fetch cluster status: %v", err)
	}

	// Assume the values are read and updated accordingly
	state.Status = "read"
	return state, nil
}

func (n *Cluster) Update(ctx *pulumi.Context, id pulumi.IDInput, args ClusterArgs) (ClusterState, error) {
	// Sort nodes for consistent order
	sortedNodes := sortNodes(args.Nodes)

	// Fetch current state
	currentState, err := n.Read(ctx, id, ClusterState{})
	if err != nil {
		return ClusterState{}, fmt.Errorf("failed to read current cluster state: %v", err)
	}

	// Get sorted current nodes
	currentNodes := sortNodes(currentState.Nodes)

	// Handle removed nodes first
	removedNodes := differenceNodes(currentNodes, sortedNodes)
	currentNodesForRemoval := currentNodes
	for _, node := range removedNodes {
		log.Info(ctx, fmt.Sprintf("Removing node %s from cluster %s\n", node.Host, args.Name))
		if err := removeNode(ctx, currentNodesForRemoval, node); err != nil {
			return ClusterState{}, fmt.Errorf("failed to remove node %s: %v", node.Host, err)
		}
		currentNodesForRemoval = filterOutNode(currentNodesForRemoval, node.Host)
	}

	// Handle added nodes
	addedNodes := differenceNodes(sortedNodes, currentNodes)
	if len(addedNodes) > 0 {
		// Use any existing node as the main node for adding new ones
		mainNode := currentNodesForRemoval[0]

		for _, node := range addedNodes {
			log.Info(ctx, fmt.Sprintf("Adding node %s to cluster %s\n", node.Host, args.Name))
			if err := addNodeToCluster(ctx, mainNode, node); err != nil {
				return ClusterState{}, fmt.Errorf("failed to add node %s: %v", node.Host, err)
			}
		}
	}

	// Update args with sorted nodes
	args.Nodes = sortedNodes

	// Return updated state
	return ClusterState{
		ClusterArgs: args,
		Status:      "updated",
	}, nil
}

// Delete removes the cluster using ConnectionArgs for SSH.
func (n *Cluster) Delete(ctx *pulumi.Context, id pulumi.IDInput, args ClusterArgs) error {
	currentNodes := args.Nodes

	// Remove nodes one by one
	for _, node := range currentNodes {
		if err := removeNode(ctx, currentNodes, node); err != nil {
			return err
		}
		// Remove this node from the currentNodes slice for next iteration
		currentNodes = filterOutNode(currentNodes, node.Host)
	}
	return nil
}

// addNodeToCluster adds a node to the PVE cluster using the provided root password.
func addNodeToCluster(ctx interface{}, mainNode Node, node Node) error {
	// Step 1: Pre-trust the SSH key of the main node
	trustKeyCmd := fmt.Sprintf("ssh-keyscan -H %s >> /root/.ssh/known_hosts", mainNode.Host)
	log.Info(ctx, fmt.Sprintf("Pre-trusting SSH host key for main node %s with command: %s", mainNode.Host, trustKeyCmd))

	if err := node.Connection.ExecuteCommand(ctx, trustKeyCmd); err != nil {
		return fmt.Errorf("failed to trust SSH key for node %s: %v", mainNode.Host, err)
	}

	log.Info(ctx, fmt.Sprintf("SSH key for main node %s successfully trusted on node %s", mainNode.Host, node.Host))

	// Step 2: Add the node to the cluster
	addNodeCmd := fmt.Sprintf("pvecm add %s --use_ssh -force", mainNode.Host)
	log.Info(ctx, fmt.Sprintf("Adding node %s to cluster with command: %s", node.Host, addNodeCmd))

	if err := node.Connection.ExecuteCommand(ctx, addNodeCmd); err != nil {
		return fmt.Errorf("failed to add node %s to the cluster: %v", node.Host, err)
	}

	log.Info(ctx, fmt.Sprintf("Node %s successfully added to the cluster", node.Host))
	return nil
}

// createCluster creates the PVE cluster on the main node, temporarily enables root password, and waits for completion.
func createCluster(ctx interface{}, input ClusterArgs) (Node, error) {
	// Set a temporary root password and enable root login.
	var mainNode = input.Nodes[0]

	// Create the cluster with the provided command.
	createClusterCmd := fmt.Sprintf("pvecm create %s", input.Name)
	log.Info(ctx, fmt.Sprintf("Running command on main node: %s", createClusterCmd))

	// Execute the command to create the cluster.
	if err := mainNode.Connection.ExecuteCommand(ctx, createClusterCmd); err != nil {
		return mainNode, fmt.Errorf("failed to create cluster on node %s: %v", mainNode.Host, err)
	}

	// Poll to check cluster status.
	if err := checkClusterStatus(ctx, mainNode); err != nil {
		return mainNode, fmt.Errorf("cluster creation did not complete or stabilize in time on node %s: %v", mainNode.Host, err)
	}

	log.Info(ctx, "Cluster creation successful and stable.")

	// Proceed with adding nodes using the temporary root password.
	for _, node := range input.Nodes[1:] {
		if err := addNodeToCluster(ctx, mainNode, node); err != nil {
			return mainNode, fmt.Errorf("failed to add node %s to the cluster: %v", node.Host, err)
		}
		log.Info(ctx, fmt.Sprintf("Node %s successfully added to the cluster", node.Host))
	}

	return mainNode, nil
}

// checkClusterStatus runs `pvecm status` to verify that the cluster is stable and fully created.
func checkClusterStatus(ctx interface{}, node Node) error {
	cmd := "pvecm status"
	log.Info(ctx, "Running cluster status check on main node")

	// Run the status check command on the main node
	if err := node.Connection.ExecuteCommand(ctx, cmd); err != nil {
		return fmt.Errorf("failed to check cluster status: %v", err)
	}

	log.Info(ctx, "Cluster status check successful.")
	return nil
}

// generateRandomPassword generates a secure random password of a given length.
func generateRandomPassword(length int) (string, error) {
	// Generate random bytes
	randomBytes := make([]byte, length)
	if _, err := rand.Read(randomBytes); err != nil {
		return "", err
	}

	// Encode to base64 to ensure it is a printable string and remove any padding characters
	return strings.TrimRight(base64.URLEncoding.EncodeToString(randomBytes), "="), nil
}

// sortNodes sorts nodes by their host names.
func sortNodes(nodes []Node) []Node {
	sort.SliceStable(nodes, func(i, j int) bool {
		return nodes[i].Host < nodes[j].Host
	})
	return nodes
}

// differenceNodes returns the nodes that are in slice1 but not in slice2.
func differenceNodes(slice1, slice2 []Node) []Node {
	m := make(map[string]bool)
	for _, item := range slice2 {
		m[item.Host] = true
	}

	var diff []Node
	for _, item := range slice1 {
		if !m[item.Host] {
			diff = append(diff, item)
		}
	}
	return diff
}

func removeNode(ctx interface{}, nodes []Node, nodeToRemove Node) error {
	// Find any remaining node that isn't being removed to execute cluster commands
	var remainingNode *Node
	for _, n := range nodes {
		if n.Host != nodeToRemove.Host {
			remainingNode = &n
			break
		}
	}

	// Step 1: Stop services and clean initial configs on target node
	// https://pve.proxmox.com/pve-docs/chapter-pvecm.html#pvecm_separate_node_without_reinstall
	stopServicesCmd := `
			systemctl stop pve-cluster &&
			systemctl stop corosync &&
			pmxcfs -l &&
			rm /etc/pve/corosync.conf &&
			rm -r /etc/corosync/* &&
			killall pmxcfs &&
			systemctl start pve-cluster
	`
	if err := nodeToRemove.Connection.ExecuteCommand(ctx, stopServicesCmd); err != nil {
		return fmt.Errorf("failed to stop services on node %s: %v", nodeToRemove.Host, err)
	}

	// If there's a remaining node, execute cluster commands on it
	if remainingNode != nil {
		delNodeCmd := fmt.Sprintf("pvecm delnode %s && rm -rf /etc/pve/nodes/%s",
			nodeToRemove.Host, nodeToRemove.Host)
		if err := remainingNode.Connection.ExecuteCommand(ctx, delNodeCmd); err != nil {
			return fmt.Errorf("failed to remove node %s from cluster: %v", nodeToRemove.Host, err)
		}
	}

	// Step 3: Final cleanup on target node
	cleanupCmd := "rm -rf /var/lib/corosync/*"
	if err := nodeToRemove.Connection.ExecuteCommand(ctx, cleanupCmd); err != nil {
		return fmt.Errorf("failed to cleanup corosync on node %s: %v", nodeToRemove.Host, err)
	}

	return nil
}

func filterOutNode(nodes []Node, hostToRemove string) []Node {
	var filtered []Node
	for _, node := range nodes {
		if node.Host != hostToRemove {
			filtered = append(filtered, node)
		}
	}
	return filtered
}
